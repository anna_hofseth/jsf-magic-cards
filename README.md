# Magic Cards

## About
This application displays "Magic the Gathering" cards. 

### Login
- username: admin
- password: admin

## Project setup
- Have Node installed
- run command:
`
npm install
`

### Development
`
npm run serve
`

### Production
`
npm run build
`