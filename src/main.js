import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import HomePage from './pages/HomePage.vue'
import LoginPage from './pages/LoginPage.vue'
import CardPage from './pages/CardPage.vue'
import AboutPage from './pages/AboutPage.vue'
import ContactPage from './pages/ContactPage.vue'
Vue.use(VueRouter)

Vue.config.productionTip = false

const router = new VueRouter({
  routes: [
    {
        path: "/:searchTerm?",
        name: "Home",
        component: HomePage,
        props: true
    },
    {
        path: "/login",
        name: "Login",
        component: LoginPage
    },
    {
        path: "/card/:id",
        name: "Card",
        component: CardPage,
        props: true

    },
    {
        path: "/about",
        name: "About",
        component: AboutPage
    },
    {
        path: "/contact",
        name: "Contact",
        component: ContactPage
    }
  ]
});

new Vue({
  render: h => h(App),
  router
}).$mount('#app')